import 'package:clima/services/location.dart';
import 'package:clima/services/networking.dart';

const weatherApiKey = '76546947dd9867af70024c08ebb26165';
String openWeatherMapUrl = 'http://api.openweathermap.org/data/2.5/weather';

class WeatherModel {
  Future getWeatherDataByCity(String city) async {
    try {
      String url =
          '$openWeatherMapUrl?q=$city&appid=$weatherApiKey&units=metric';
      NetWorkingHelper network = NetWorkingHelper(url);

      var weatherData = await network.getData();
      return weatherData;
    } catch (e) {
      print('e: $e');
    }
  }

  Future getWeatherData() async {
    try {
      Location location = new Location();
      await location.getLocation();

      double latitude = location.latitude;
      double longitude = location.longitude;

      String url =
          '$openWeatherMapUrl?lon=$longitude&lat=$latitude&appid=$weatherApiKey&units=metric';
      NetWorkingHelper network = NetWorkingHelper(url);

      var weatherData = await network.getData();

      return weatherData;
    } catch (e) {
      print('e: $e');
    }
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
