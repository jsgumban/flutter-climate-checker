import 'package:http/http.dart' as http;
import 'dart:convert';

class NetWorkingHelper {
  NetWorkingHelper(this.url);
  final String url;

  Future getData() async {
    http.Response response = await http.get(url);
    var data = jsonDecode(response.body);

    return data;
  }
}
